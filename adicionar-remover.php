<!doctype html>
<html class="no-js" lang="en" ng-app="myApp">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Angular</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <script src="js/vendor/modernizr.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
  </head>
  <body ng-controller="CtrlLista">
    <?php include 'header.html'; ?>
    <br>

    <form>
      <div class="row">
        <div class="large-12 columns">
          <label>Busca</label>
          <input type="text" ng-model="busca" />
        </div>
      </div>
    </form>

    <div class="row">
        <div class="large-12 columns">
          <ul class="small-block-grid-3">
            <li ng-repeat="pessoa in pessoas | filter:busca">{{pessoa.nome}}, {{pessoa.cidade}} <button ng-click="remover($index);" class="small button">Remover</button></li>
          </ul>
        </div>
    </div>

    <div class="row">
        <div class="large-12 columns">
          <p><label>Nome:</label> <input type="text" ng-model="vNome"></p>
          <p><label>Cidade:</label> <input type="text" ng-model="vCidade"></p>
          <button ng-click="adicionar();" class="small button">Adicionar a lista</button>
        </div>
    </div>
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>

    <script>
    $(document).foundation();


    (function(angular){
        'use strict';
        //cria a aplicacao
        var myApp = angular.module('myApp',[]);
        //cria o controller
        myApp.controller('CtrlLista',function($scope){
            $scope.pessoas = [
                {nome: 'Maria', cidade: 'São Paulo'},
                {nome: 'Julia', cidade: 'São Paulo'},
                {nome: 'Flávia', cidade: 'Rio de Janeiro'}
            ];
            $scope.adicionar = function() {
                $scope.pessoas.push({
                    nome: $scope.vNome, cidade: $scope.vCidade
                });
                $scope.vNome = "";
                $scope.vCidade = "";
            };
            $scope.remover = function(index) {
               $scope.pessoas.splice(index,1);
            };
        });
    })(window.angular);
    </script>
  </body>
</html>
