(function(angular){
    'use strict';
    //cria a aplicacao
    var myApp = angular.module('myApp',['ngRoute','ngResource']);

    //cria as configuracoes das rotas
    myApp.config(
        [
            '$routeProvider',
            function($routeProvider) {
                $routeProvider
                    .when('/', {
                        templateUrl: 'template.html',
                        controller: 'Ctrl'
                    })
                ;
            }
        ]
    );

    //cria o controller de listagem
    myApp.controller('Ctrl',function($scope, pessoasRepository){
        
        pessoasRepository.getAll().success(function(pessoas) {
            $scope.pessoas = pessoas.pessoas;
        });
    });

    myApp.factory('pessoasRepository', function($http) {
        return {
            getAll: function() {
                var url = "pessoas.json";
                return $http.get(url);
            }
        };
    });


})(window.angular);