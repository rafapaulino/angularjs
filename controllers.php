<!doctype html>
<html class="no-js" lang="en" ng-app="myApp">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Angular</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <script src="js/vendor/modernizr.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
  </head>
  <body>
    <?php include 'header.html'; ?>
    <br>
    <div class="row">
      <div class="large-12 columns">
        <div class="panel" ng-controller="CtrlApp">
          <h3>Olá {{nome}}! </h3>
          <p><strong>Você mora em:</strong> {{cidade}} e tem {{idade}} anos.</p>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="large-12 columns">
        <div class="panel" ng-controller="CtrlApp2">
          <h3>Olá {{nome}}! </h3>
          <p><strong>Você mora em:</strong> {{cidade}} e tem {{idade}} anos.</p>
        </div>
      </div>
    </div>
    
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();

        (function(angular){
            'use strict';
            //cria a aplicacao
            var myApp = angular.module('myApp',[]);
            //cria o controller
            myApp.controller('CtrlApp',function($scope){
                $scope.nome = "Rafael";
                $scope.cidade = "São Paulo";
                $scope.idade = 31;
                //criando um watch - funciona como um event listener
                $scope.$watch('nome',function (){
                    console.log($scope.nome);
                });
            });
            //cria o segundo controller
            myApp.controller('CtrlApp2',function($scope){
                $scope.nome = "Mariana";
                $scope.cidade = "São Paulo";
                $scope.idade = 25;
                $scope.cidade = "Mairiporã";
            });
        })(window.angular);
    </script>
  </body>
</html>
