(function(angular){
    'use strict';
    //cria a aplicacao
    var myApp = angular.module('myApp',['ngRoute']);

    //cria as configuracoes das rotas
    myApp.config(
        [
            '$routeProvider',
            function($routeProvider) {
                $routeProvider
                    .when('/', {
                        templateUrl: 'listar.html'
                    })
                    .when('/adicionar', {
                        templateUrl: 'adicionar.html',
                        controller: 'CtrlAdd'
                    })
                    .when('/editar/:index', {
                        templateUrl: 'editar.html',
                        controller: 'CtrlEdit'
                    })
                ;
            }
        ]
    );

    //cria o controller de listagem
    myApp.controller('CtrlPeople',function($scope){
        $scope.pessoas = [
            {nome: 'Maria', cidade: 'São Paulo'},
            {nome: 'Julia', cidade: 'São Paulo'},
            {nome: 'Flávia', cidade: 'Rio de Janeiro'}
        ];
        $scope.remover = function(index) {
           $scope.pessoas.splice(index,1);
        };
    });

    myApp.controller('CtrlAdd',function($scope){
        
        $scope.Alert = false;
        $scope.adicionar = function() {
            $scope.pessoas.push($scope.pessoa);
            $scope.pessoa = "";
            $scope.result = "Registro Adicionado com sucesso!";
            $scope.Alert = true;
        };
    });

    myApp.controller('CtrlEdit',function($scope, $routeParams){
        
        $scope.Alert = false;
        $scope.pessoa = $scope.pessoas[$routeParams.index];
        $scope.editar = function() {
            
            
            $scope.result = "Registro Editado com sucesso!";
            $scope.Alert = true;
        };
    });


})(window.angular);