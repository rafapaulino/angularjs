<!doctype html>
<html class="no-js" lang="en" ng-app="myApp">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Angular</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <script src="js/vendor/modernizr.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
  </head>
  <body>
  <?php include 'header.html'; ?>
  <br> 
    <div class="row">
      <div class="large-12 columns" ng-controller="Ctrl">
        <h1>Olá {{yourName | case: 'lower'}}</h1>
      </div>
    </div>
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
    $(document).foundation();

    //angular
    (function(angular){
        'use strict';
        //cria a aplicacao
        var myApp = angular.module('myApp',[]);
            //filtro
            myApp.filter('case', function(){
                return function(input, type){
                  if (type === 'upper')
                  return input.toUpperCase();
                  else
                  return input.toLowerCase();
                }
            });
            //controller
            myApp.controller('Ctrl',function($scope){
                $scope.yourName = "Rafael gente fina!";
            });

    })(window.angular);
    </script>
  </body>
</html>
