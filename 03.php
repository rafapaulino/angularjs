<!doctype html>
<html class="no-js" lang="en" ng-app="myApp">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Angular</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <script src="js/vendor/modernizr.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
  </head>
  <body>
    <?php include 'header.html'; ?>
    <br>

    <div class="row" ng-controller="Ctrl">
        <div class="large-12 columns">
          <h3>{{nome}}</h3>
        </div>
    </div>

    <div class="row" ng-controller="Ctrl2">
        <div class="large-12 columns">
          <h3>{{nome}}</h3>
        </div>
    </div>
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>

    <script>
    $(document).foundation();

    (function(angular){
        'use strict';
        //cria a aplicacao
        var myApp = angular.module('myApp',[]);
        //cria o controller
        var MyController = function(s) {
          s.nome = "Rafael";
        }
        MyController.$inject = ['$scope'];
        myApp.controller('Ctrl', MyController);

        //criando outro controlador com dependencia de uma maneira diferente
        myApp.controller('Ctrl2', ['$scope', function(s){
            s.nome = "Rafael Lindão!";
        }]);
        
    })(window.angular);
    </script>
  </body>
</html>
