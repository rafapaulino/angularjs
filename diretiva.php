<!doctype html>
<html class="no-js" lang="en" ng-app="myApp">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Angular</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <script src="js/vendor/modernizr.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
  </head>
  <body>
    <?php include 'header.html'; ?>
    <br>
    <div class="row">
      <div class="large-12 columns" ng-controller="Ctrl">
        <button son-click="executa()" class="small radius button">Click</button>
        <son-clic click="executa()" class="small radius button alert">Clic</son-clic>
      </div>
    </div>
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
    $(document).foundation();

    //angular
    (function(angular){
        'use strict';
        //cria a aplicacao
        var myApp = angular.module('myApp',[]);

           myApp.directive('sonClick', function() {
                return {
                    restrict: 'A',
                    link: function(scope, element, attrs) {
                      //executa o valor do atributo
                        element.bind('click', function() {
                            scope.$eval(attrs.sonClick);
                        })
                    }
                };
            });

            myApp.directive('sonClic', function() {
                return {
                    restrict: 'E',
                    replace: true,
                    transclude: true,
                    template: '<button ng-transclude></button>',
                    link: function(scope, element, attrs) {
                        //executa o valor do atributo click
                        element.bind('click', function() {
                            scope.$eval(attrs.click);
                        })
                    }
                };
            });

            //controller
            myApp.controller('Ctrl',function($scope){
                $scope.executa = function(){
                    console.log('Diretiva clicada!');
                }
            });

    })(window.angular);
    </script>
  </body>
</html>
