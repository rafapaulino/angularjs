<!doctype html>
<html class="no-js" lang="en" ng-app>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Angular</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <script src="js/vendor/modernizr.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
  </head>
  <body>
    <?php include 'header.html'; ?>
    <br>
    <div class="row">
      <div class="large-12 columns">
        <h1>Olá {{yourName}}</h1>
      </div>
    </div>
              
    <form>
		  <div class="row">
		    <div class="large-12 columns">
		      <label>Coloque seu nome no campo abaixo:</label>
		      <input type="text" placeholder="Coloque seu nome aqui..." ng-model="yourName" />
		    </div>
		  </div>
		</form>
      
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
