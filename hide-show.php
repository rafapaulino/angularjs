<!doctype html>
<html class="no-js" lang="en" ng-app>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Angular</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <script src="js/vendor/modernizr.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.16/angular.min.js"></script>
  </head>
  <body>
    <?php include 'header.html'; ?>
    <br>

    <div class="row">
        <div class="large-4 medium-4 columns">
          <label>Show</label>
          <input type="text" ng-model="valor"><input type="checkbox" ng-model="checado">
          <a href="#" ng-show="checado" class="medium success button">{{valor}}</a><br/>
        </div>
        <div class="large-4 medium-4 columns">
          <label>Hide</label>
          <input type="text" ng-model="valor2"><input type="checkbox" ng-model="checado2">
          <a href="#" ng-hide="checado2" class="medium alert button">{{valor2}}</a><br/>
        </div>
    </div>
     
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
